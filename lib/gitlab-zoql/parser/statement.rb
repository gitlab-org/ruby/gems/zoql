# frozen_string_literal: true

module GitlabZOQL::Parser::Statement
  class Node
    def accept(visitor)
      klass = self.class.ancestors.find do |ancestor|
        visitor.respond_to?("visit_#{demodulize(ancestor.name)}")
      end

      raise "No visitor for #{self.class.name}" unless klass

      visitor.__send__("visit_#{demodulize(klass.name)}", self)
    end

    def to_sql
      GitlabZOQL::Visitor.new.visit(self)
    end

    private

    def demodulize(str)
      str.split('::')[-1]
    end
  end

  class Identifier < Node
    def initialize(name)
      @name = name
    end

    attr_reader :name
  end

  class Select < Node
    def initialize(list, table_expression = nil)
      @list = list
      @table_expression = table_expression
    end

    attr_reader :list, :table_expression
  end

  class SelectList < Node
    def initialize(columns)
      @columns = Array(columns)
    end

    attr_reader :columns
  end

  class DirectSelect < Node
    def initialize(query_expression)
      @query_expression = query_expression
    end

    attr_reader :query_expression
  end

  class WhereClause < Node
    def initialize(search_condition)
      @search_condition = search_condition
    end

    attr_reader :search_condition
  end

  class TableExpression < Node
    def initialize(from_clause, where_clause = nil)
      @from_clause = from_clause
      @where_clause = where_clause
    end

    attr_reader :from_clause, :where_clause
  end

  class FromClause < Node
    def initialize(tables)
      @tables = Array(tables)
    end

    attr_reader :tables
  end

  class Table < Identifier
  end

  class Column < Identifier
  end

  class ComparisonPredicate < Node
    def initialize(left, right)
      @left = left
      @right = right
    end

    attr_reader :left, :right
  end

  class GreaterOrEquals < ComparisonPredicate
  end

  class LessOrEquals < ComparisonPredicate
  end

  class Greater < ComparisonPredicate
  end

  class Less < ComparisonPredicate
  end

  class Equals < ComparisonPredicate
  end

  class SearchCondition < Node
    def initialize(left, right)
      @left = left
      @right = right
    end

    attr_reader :left, :right
  end

  class Or < SearchCondition
  end

  class And < SearchCondition
  end

  class Unary < Node
    def initialize(value)
      @value = value
    end

    attr_reader :value
  end

  class Not < Unary
  end

  class True < Node
  end

  class False < Node
  end

  class Null < Node
  end

  class Literal < Node
    def initialize(value)
      @value = value
    end

    attr_reader :value
  end

  class DateTime < Literal
  end

  class String < Literal
  end

  class Float < Literal
  end

  class Integer < Literal
  end
end
