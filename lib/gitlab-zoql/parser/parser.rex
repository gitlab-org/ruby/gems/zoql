# -*- fill-column: 30; -*-

class GitlabZOQL::Parser

option
  ignorecase

macro
  DIGIT       [0-9]
  UINT        {DIGIT}+
  BLANK       \s+

  YEARS       {UINT}
  MONTHS      {UINT}
  DAYS        {UINT}
  HOURS       {UINT}
  MINUTES     {UINT}
  SECONDS     {UINT}
  TIME        {HOURS}:{MINUTES}:{SECONDS}
  OFFSET      \+{UINT}:{UINT}
  DATE        {YEARS}-{MONTHS}-{DAYS}
  DATETIME    {DATE}T{TIME}{OFFSET}

  IDENT       \w+

rule
# [:state]    pattern                   [actions]
# literals
              \'                        { @state = :STRS;  [:quote, text] }
  :STRS       {DATETIME}                { [:datetime_string, DateTime.parse(text)] }
  :STRS       (?:[^\\']|\\.|'')+        {                  [:character_string_literal, text] }
  :STRS       \'                        { @state = nil;    [:quote, text] }

              {UINT}                    { [:unsigned_integer, text.to_i] }
# skip
              {BLANK}                   # no action

# keywords
              AND\b                       { [:AND, text] }
              COUNT\b                     { [:COUNT, text] }
              ELSE\b                      { [:ELSE, text] }
              FROM\b                      { [:FROM, text] }
              NULL\b                      { [:NULL, text] }
              OR\b                        { [:OR, text] }
              SELECT\b                    { [:SELECT, text] }
              WHERE\b                     { [:WHERE, text] }

# tokens
              !=                          { [:not_equals_operator, text] }
              =                           { [:equals_operator, text] }
              <=                          { [:less_than_or_equals_operator, text] }
              <                           { [:less_than_operator, text] }
              >=                          { [:greater_than_or_equals_operator, text] }
              >                           { [:greater_than_operator, text] }
              ,                           { [:comma, text] }

# identifier
              {IDENT}                     { [:identifier, text] }

---- header ----
require 'date'
