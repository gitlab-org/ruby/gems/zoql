# frozen_string_literal: true

module GitlabZOQL
  class Visitor
    def initialize
      @negated = false
    end

    def visit(node)
      node.accept(self)
    end

    def visit_DirectSelect(o)
      [
        o.query_expression
      ].compact.collect { |e| visit(e) }.join(' ')
    end

    def visit_Select(o)
      "SELECT #{visit_all([o.list, o.table_expression].compact).join(' ')}"
    end

    def visit_SelectList(o)
      arrayize(o.columns)
    end

    def visit_TableExpression(o)
      [
        o.from_clause,
        o.where_clause
      ].compact.collect { |e| visit(e) }.join(' ')
    end

    def visit_FromClause(o)
      "FROM #{arrayize(o.tables)}"
    end

    def visit_WhereClause(o)
      "WHERE #{visit(o.search_condition)}"
    end

    def visit_Or(o)
      search_condition('OR', o)
    end

    def visit_And(o)
      search_condition('AND', o)
    end

    def visit_GreaterOrEquals(o)
      comparison('>=', o)
    end

    def visit_LessOrEquals(o)
      comparison('<=', o)
    end

    def visit_Greater(o)
      comparison('>', o)
    end

    def visit_Less(o)
      comparison('<', o)
    end

    def visit_Equals(o)
      if @negated
        comparison('!=', o)
      else
        comparison('=', o)
      end
    end

    def visit_Table(o)
      o.name
    end

    def visit_Column(o)
      o.name
    end

    def visit_DateTime(o)
      "'%s'" % escape(o.value.strftime('%Y-%m-%d %H:%M:%S'))
    end

    def visit_String(o)
      '%s' % escape(o.value)
    end

    def visit_Float(o)
      o.value.to_s
    end

    def visit_Integer(o)
      o.value.to_s
    end

    def visit_Literal(o)
      quote(o.value)
    end

    def visit_Identifier(o)
      o.name
    end

    def visit_Not(o)
      negate { visit(o.value) }
    end

    def visit_True(_o)
      'TRUE'
    end

    def visit_False(_o)
      'FALSE'
    end

    def visit_Null(_o)
      'NULL'
    end

    private

    def negate
      @negated = true
      yield
    ensure
      @negated = false
    end

    def quote(str)
      "'#{str}'"
    end

    def escape(str)
      str.gsub(/'/, "''")
    end

    def comparison(operator, o)
      [visit(o.left), operator, visit(o.right)].join(' ')
    end

    def search_condition(operator, o)
      "#{visit(o.left)} #{operator} #{visit(o.right)}"
    end

    def visit_all(nodes)
      nodes.collect { |e| visit(e) }
    end

    def arrayize(arr)
      visit_all(arr).join(', ')
    end
  end
end
