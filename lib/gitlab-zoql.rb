# frozen_string_literal: true

require_relative 'gitlab-zoql/version'

module GitlabZOQL
  require_relative 'gitlab-zoql/parser/parser.racc'
  require_relative 'gitlab-zoql/parser/parser.rex'
  require_relative 'gitlab-zoql/parser/statement'
  require_relative 'gitlab-zoql/visitor'
end
