# frozen_string_literal: true

$LOAD_PATH.unshift File.expand_path('../lib', __dir__)

begin
require 'simplecov'
SimpleCov.start do
  add_filter "/test/"
  enable_coverage :branch
end
rescue LoadError
end

require 'gitlab-zoql'

require 'minitest/autorun'
require 'minitest/focus'
require 'minitest/reporters'
Minitest::Reporters.use!
