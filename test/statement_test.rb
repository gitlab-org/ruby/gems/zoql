# frozen_string_literal: true

require_relative 'helper'

class StatementTest < Minitest::Test
  def test_simple_select
    select_list = GitlabZOQL::Parser::Statement::SelectList.new(
      [GitlabZOQL::Parser::Statement::Column.new('AccountId'),
       GitlabZOQL::Parser::Statement::Column.new('FirstName'),
       GitlabZOQL::Parser::Statement::Column.new('LastName')]
    )

    table_expresion = GitlabZOQL::Parser::Statement::TableExpression.new(
      GitlabZOQL::Parser::Statement::FromClause.new(
        GitlabZOQL::Parser::Statement::Table.new('Account')
      )
    )

    assert_sql('SELECT AccountId, FirstName, LastName FROM Account',
               GitlabZOQL::Parser::Statement::DirectSelect.new(
                 GitlabZOQL::Parser::Statement::Select.new(
                   select_list, table_expresion
                 )
               ))
  end

  def test_simple_select_list_with_single_condition
    select_list = GitlabZOQL::Parser::Statement::SelectList.new(
      [GitlabZOQL::Parser::Statement::Column.new('AccountId')]
    )

    where_clause = GitlabZOQL::Parser::Statement::WhereClause.new(
      GitlabZOQL::Parser::Statement::Equals.new(
        GitlabZOQL::Parser::Statement::Identifier.new('State'),
        GitlabZOQL::Parser::Statement::Literal.new('California')
      )
    )

    table_expresion = GitlabZOQL::Parser::Statement::TableExpression.new(
      GitlabZOQL::Parser::Statement::FromClause.new(GitlabZOQL::Parser::Statement::Table.new('contact')),
      where_clause
    )

    assert_sql('SELECT AccountId FROM contact WHERE State = \'California\'',
               GitlabZOQL::Parser::Statement::DirectSelect.new(
                 GitlabZOQL::Parser::Statement::Select.new(
                   select_list, table_expresion
                 )
               ))
  end

  def test_simple_select_with_multiple_and_conditions
    select_list = GitlabZOQL::Parser::Statement::SelectList.new(
      [GitlabZOQL::Parser::Statement::Column.new('AccountNumber')]
    )

    where_clause = GitlabZOQL::Parser::Statement::WhereClause.new(
      GitlabZOQL::Parser::Statement::And.new(
        GitlabZOQL::Parser::Statement::Equals.new(
          GitlabZOQL::Parser::Statement::Identifier.new('Status'),
          GitlabZOQL::Parser::Statement::Literal.new('New')
        ),
        GitlabZOQL::Parser::Statement::GreaterOrEquals.new(
          GitlabZOQL::Parser::Statement::Identifier.new('BillCycleDay'),
          GitlabZOQL::Parser::Statement::Integer.new(15)
        )
      )
    )

    table_expresion = GitlabZOQL::Parser::Statement::TableExpression.new(
      GitlabZOQL::Parser::Statement::FromClause.new(GitlabZOQL::Parser::Statement::Table.new('account')),
      where_clause
    )

    assert_sql("SELECT AccountNumber FROM account WHERE Status = 'New' AND BillCycleDay >= 15",
               GitlabZOQL::Parser::Statement::DirectSelect.new(
                 GitlabZOQL::Parser::Statement::Select.new(
                   select_list, table_expresion
                 )
               ))
  end

  def test_integer
    assert_sql '1', GitlabZOQL::Parser::Statement::Integer.new(1)
  end

  private

  def assert_sql(expected, ast)
    assert_equal expected, ast.to_sql
  end
end
