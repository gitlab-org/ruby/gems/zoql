# frozen_string_literal: true

require_relative 'helper'

class ParserTest < Minitest::Test
  def test_simple_select
    parsed = GitlabZOQL::Parser.parse('select AccountNumber from Account').to_sql

    assert_equal('SELECT AccountNumber FROM Account', parsed)
  end

  def test_simple_select_list
    parsed = GitlabZOQL::Parser.parse('select AccountId, FirstName, LastName from contact').to_sql

    assert_equal('SELECT AccountId, FirstName, LastName FROM contact', parsed)
  end

  def test_select_sublist
    parsed = GitlabZOQL::Parser.parse("select AccountId,OriginalId from Subscription where AccountId='0c07bf87c30e64d35ecb41c07df16f76' and Status='Active'").to_sql

    assert_equal(
      "SELECT AccountId, OriginalId FROM Subscription WHERE AccountId = '0c07bf87c30e64d35ecb41c07df16f76' AND Status = 'Active'", parsed
    )
  end

  def test_simple_select_list_with_single_condition
    parsed = GitlabZOQL::Parser.parse('select AccountId, FirstName, LastName from contact where State = \'California\'').to_sql

    assert_equal('SELECT AccountId, FirstName, LastName FROM contact WHERE State = \'California\'', parsed)
  end

  def test_simple_select_with_multiple_and_conditions
    parsed = GitlabZOQL::Parser.parse("select AccountNumber from account where Status = 'New' and BillCycleDay >= 15").to_sql

    assert_equal("SELECT AccountNumber FROM account WHERE Status = 'New' AND BillCycleDay >= 15", parsed)
  end

  def test_simple_select_with_negation_condition
    parsed = GitlabZOQL::Parser.parse('select AccountNumber from account where AutoPay != true').to_sql

    assert_equal('SELECT AccountNumber FROM account WHERE AutoPay != true', parsed)
  end

  def test_simple_select_with_datetime_condition
    parsed = GitlabZOQL::Parser.parse("select AccountNumber from account where CreatedDate <= '2012-09-12T01:00:00'").to_sql

    assert_equal("SELECT AccountNumber FROM account WHERE CreatedDate <= '2012-09-12T01:00:00'", parsed)
  end

  def test_escape
    parsed = GitlabZOQL::Parser.parse("select AccountNumber from account where Name = 'San Francisco\\'s Finest'").to_sql

    assert_equal("SELECT AccountNumber FROM account WHERE Name = 'San Francisco\\'s Finest'", parsed)
  end

  def test_not_equals_null
    parsed = GitlabZOQL::Parser.parse("select AccountNumber from account where PurchaseOrderNumber != null").to_sql

    assert_equal("SELECT AccountNumber FROM account WHERE PurchaseOrderNumber != NULL", parsed)
  end

  def test_equals_null
    parsed = GitlabZOQL::Parser.parse("select AccountNumber from account where PurchaseOrderNumber = null").to_sql

    assert_equal("SELECT AccountNumber FROM account WHERE PurchaseOrderNumber = NULL", parsed)
  end
end
