# frozen_string_literal: true

require_relative 'lib/gitlab-zoql/version'

Gem::Specification.new do |spec|
  spec.name = 'gitlab-zoql'
  spec.version = GitlabZOQL::VERSION
  spec.authors = ['Vitaly Slobodin']
  spec.email = ['vslobodin@gitlab.com']

  spec.summary = 'A toolkit for working with ZOQL language'
  spec.description = 'ZOQL language parser and AST explorer.'
  spec.homepage = 'https://gitlab.com/gitlab-org/ruby/gems/gitlab-zoql'
  spec.license = 'MIT'
  spec.required_ruby_version = '>= 3.0.0'

  spec.metadata['rubygems_mfa_required'] = 'true'

  spec.files = Dir['lib/**/*.rb']
  spec.require_paths = ['lib']

  spec.add_development_dependency 'minitest', '~> 5.16'
  spec.add_development_dependency 'minitest-focus', '~> 1.4'
  spec.add_development_dependency 'minitest-reporters', '~> 1.6'
  spec.add_development_dependency 'rake', '~> 13.0'
  spec.add_development_dependency 'rexical', '~> 1.0'
end
